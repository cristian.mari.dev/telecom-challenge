# Aclaraciones
Realizo el challenge para postular como front-end dev

( Por cuestiones de tiempo realice unicamente el challenge del front, tengo conocimientos en node y php de back pero apuesto a la posición de Front)

El fondo de la cabecera cambia segun el horario del día.

Suelo usar clases en contenedores y hooks en componentes, me parecen claros, no participe en desarrollos a puro hooks, si en proyectos propios.

Me habria gustado separar como entidades los servicios lo haria muy claro en los controladores.

No pude conectar y probar en estos dias con el celular el responsive asumi las medidas del navegador proporciona para las pruebas podrían ayudar, un bug se presento en mobile, stoppeo los avances del challenge para fines de examenes completos a pesar del error.

# T-Clima

Aplicación para informe del clima de *argentina*.

## Comenzando

Estas instrucciones le proporcionarán una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba.

### Pre-requisitos

Qué cosas necesita para instalar la aplicación y cómo instalarlas

* [Instalar Git](/git-readme.md)

* [Instalar Node](https://nodejs.org/es/download/)


### Instalación

Primero realizar la clonación del proyecto en una carpeta destinada al desarrollo de las aplicaciones.

En sua consola ubiquese dentro de la carpeta destino y ejecute el siguiente comando en su consola
```
git clone https://gitlab.com/cristian.mari.dev/telecom.git
```


### Instalar dependencias

Abra una terminal y ubiquese dentro de la carpeta que se clono del proyecto, ejecute el siguiente comando:
```
npm install
```


## Comandos de la aplicación

| Comando  |  Descripción |  
|---|---|
| dev  |  Inicializa la aplicación en modo desarrollo tendrá actualización en vivo y funcionalidades que le ayudaran con sus tareas.  |  
|  build |  Inicializa la construcción del empaquetado de su sitio web optimizado para ambitos productivos. | 
|  test |  Inicializa las pruebas unitarias a la aplicación de react. | 


### Ejemplo del uso de los comandos:

```
npm run dev (o) yarn dev << Si tiene usa yarn >>
npm run build (o) yarn build  << Si tiene usa yarn >>
```

