/**
 * @desc Dependencias
 */
const Path              = require( 'path' );
const Webpack           = require( 'webpack' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );

/**
 * @desc Exportamos la configuración de webpack.
 */
module.exports = {

  /**
   * @desc Observa los cambios del webpack.
   */
  watch: true,

  /**
   * @desc Mapa del codigo
   */
  devtool: "cheap-module-eval-source-map",

  /**
   * @desc Archivo principal de la aplicación.
   */
  entry: [ '@babel/polyfill', Path.resolve( __dirname, './src/root.jsx' )],

  /**
   * @desc Configuración de salida.
   */
  output: {

    /**
     * @desc Carpeta de salida
     */
    path: Path.resolve( __dirname, './dist' ),

    /**
     * @desc Nombre del archivo compilado.
     */
    filename: 'assets/js/bundle.js',

    publicPath: '/'

  },

  /**
   * @desc Resolvemos las extensiones comunes
   */
  resolve: {
    extensions: [ '.js', '.jsx', '.css', '.scss', '.json']
  },

  /**
   * @desc Modulos
   */
  module: {
    rules: [

      /**
       * @desc Loader: Js
       */
      {
        test: /\.m?(js|jsx)$/,
        include: Path.join(__dirname, '/src'),
        exclude: /(node_modules|bower_components)/,
        use: 'babel-loader'
      },

      /**
       * @desc Url Loader
       * @type's images, fonts
       */
      {
        test: /\.( png | jpg | jpeg | gif | svg | ttf | woff | eot )$/i, 
        use: 'url-loader'
      },

      /**
       * @desc File Loader
       */
      {
        test: /\.(jpe?g|png|gif|svg|)$/i,
        loader: 'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        options: {
          name: '[name].[ext]',
          outputPath: './assets/img'
        }
      },
      
      /**
       * @desc File Loader
       */
      {
        test: /\.(ttf|eot|svg|gif|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
        options: {
          name: '[name].[ext]',
          outputPath: './assets/css/fonts'
        }
      }]

    },
  
  /**
   * @desc Recarga en vivo.
   */
  devServer:{

    // Observa las modificaciones de la carpeta public.
    watchContentBase: true,

    // Contenido servido.
    contentBase: Path.join(__dirname, 'dist'),

    // Archivo de entrada para el servidor.
    index: 'index.html',

    // Puerto
    port: 8080,

    // Recarga en vivo.
    hot: true,

    // Fallback del api de history
    historyApiFallback: true,

    // Esta propiedad activa la apertura de
    // el navegador al estar compilando.
    open: true
    
  },

  /**
   * @desc Plugin's
   */
  plugins: [

    /**
     * @desc Copiamos los lenguajes
     */
    new CopyWebpackPlugin([{
          from: Path.resolve(__dirname,'template/'),
          to: Path.resolve(__dirname,'dist/')
    }]),

    /**
     * @desc Genera el index.html
     */
    new HtmlWebpackPlugin({

      /**
       * @desc Titulo
       */
      title: 'Telecom',

      /**
       * @desc Nombre del archivo de salida.
       */
      filename: 'index.html',

      /**
       * @desc Template
       */
      template: Path.resolve( __dirname, 'template/index.html' ),

    }),

    /**
     * @desc Actualización en vivo.
     */
    new Webpack.HotModuleReplacementPlugin()

  ]
};
