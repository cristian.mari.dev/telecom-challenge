/**
 * @desc Dependencias
 */
import { Component } from 'react';
import Moment from "moment";

/**
 * @desc Modelo
 */
import WeatherModel from "../../Models/WeatherModel";
import IPModel from "../../Models/IPModel";

/**
 * @desc Utilidades
 */
import { KelvinToCelsius } from "../../Utils/Temperature";
import { getRandomArray } from "../../Utils/Array";

/**
 * @desc Contenedor de la pantalla de la aplicación
 */
class AppUI extends Component{

	/**
	 * @desc Constructor
	 * 
	 * @param { Object } props
	 * 
	 * @return { void }
	 */
	constructor( props ){

		super( props );

		// Estado
		this.state = {

			// Precarga
			loading: true,

			// Datos del cliente
			client : null,

			// Temperatura actual
			currentWeather: null,

			// Pronostico extendido
			forecast: null,

			// Clima en ciudades cercanas
			citysNearby: null

		};

	}

	/**
	 * @desc Ciclo de vida: Componente montado
	 * 
	 * @return { Promise<void> }
	 */
	async componentDidMount(){

		// Obtenemos la información del cliente
		await this.getClientData( async () => {

			// Indicamos el estado de la carga
			const loading = false;

			// Obtenemos el estado del tiempo actual.
			await this.getWeatherCurrent( this.state.client.city );

			// Obtenemos el pronostico del tiempo futuro.
			await this.getForecast();

			// Obtenemos el pronostico de otras ciudades.
			await this.getWeatherOfOtherCitys();

			// Actualizamos el estado
			this.setState({ loading });

		});

	}

	/**
	 * @desc Cambiamos la ciudad.
	 * 
	 * @param { String } name 
	 * 
	 * @return { Promise<Boolean> }
	 */
	async changeCity( name ){

		try{

			// Verificamos el tipo
			if( typeof name !== "string" ){
				throw new TypeError( "expected a string, but received a " + typeof name );
			}

			// Ciudad actual
			const currentCity = name;
			// Indicamos que la carga se muestre
			let loading = true;

			// Actualizamos el estado.
			this.setState({ loading });

			// Obtenemos el estado del tiempo actual.
			await this.getWeatherCurrent( currentCity );

			// Obtenemos el pronostico del tiempo futuro.
			await this.getForecast();

			// Obtenemos el pronostico de otras ciudades.
			await this.getWeatherOfOtherCitys();

			// Indicamos que se oculte la carga
			loading = false;

			// Actualizamos el estado
			this.setState({ currentCity, loading }, () => {
				window.scrollTo(0, 0);
			});

			return true;

		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return false;

		}

	}

	/**
	 * @desc Obtenemos los datos del cliente (ip, ubicación,etc.)
	 * 
	 * @param { Function } callback
	 * 
	 * @return { Promise<Object> }
	 */
	async getClientData( callback = () => {} ){

		try{
			
			// Obtenemos los datos.
			const response = await IPModel.getIp();

			// Datos del cliente
			const client = {
				"country"	: response.country_name,
				"province"	: response.region,
				"city"		: response.city,
				"cp"		: response.postal,
				"coords"	: {
					"lat"	: response.latitude,
					"lng"	: response.longitude
				},
				"ip"		: response.ip
			};

			// Ciudad actual
			let currentCity = client.city;

			// Actualizamos el estado
			this.setState({ client, currentCity }, () => typeof callback === "function" && callback( client, currentCity ) );

			return true;

		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return false;

		}

	}

	/**
	 * @desc Obtenemos los datos del tiempo actual.
	 * 
	 * @param { String } city
	 * 
	 * @return { Promise<Object> }
	 */
	async getWeatherCurrent( city ){

		try{
	
			// Verificamos la ciudad
			if( typeof city !== "string" ){
				throw new TypeError( "expected a string, but received a " + typeof city );
			}

			// Obtenemos los datos.
			const response = await WeatherModel.getCurrentWeather( city );

			// Verificamos los datos de la respuesta.
			if( response.cod === 200 ){

				// Datos del cliente
				const currentWeather = {
					// Estado escrito
					"status": response?.weather[0]?.description,

					// Estado escrito
					"sunrise": response.sys.sunrise,

					// Velocidad del viento
					"wind": response.wind.speed,

					// Temperatura
					"temp": response.main.temp,

					// Humedad
					"humidity": response.main.humidity,

					// Presion
					"sunset": response.sys.sunset
				};

				// Actualizamos el estado
				this.setState({ currentWeather });

			}

			return true;

		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return false;

		}

	}

	/**
	 * @desc Obtenemos el pronostico de otras ciudades.
	 * 
	 * @return { Promise<Object> }
	 */
	async getWeatherOfOtherCitys( client ){

		try{

			// Verificamos el cliente
			if( !client ){
				client = this.state.client;
			}

			// Verificamos la ciudad
			if( typeof client !== "object" || !(client?.coords?.lat) && !(client?.coords?.lng) ){
				throw new TypeError( "expected a object with coords (lat && lng), but received a " + typeof client );
			}

			// Obtenemos los datos.
			const response = await WeatherModel.getWeatherOfCitysNearby( client.coords.lat, client.coords.lng );

			// Verificamos los datos de la respuesta.
			if( response.cod === "200" ){

				// Ciudades random
				const citysRandom = getRandomArray( response.list, 5 );

				// Ciudades cercanas
				const citysNearby = citysRandom.map( city => ({ 
					"name": city?.name,
					"temp": KelvinToCelsius(city?.main?.temp) + " c"
				}));
				
				// Actualizamos el estado
				this.setState({ citysNearby });

			}

			return true;

		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return false;

		}

	}

	/**
	 * @desc Obtenemos los datos del pronostico futuro.
	 * 
	 * @return { Promise<Object> }
	 */
	async getForecast( currentCity ){

		try{
			
			// Alias del estado
			if( !currentCity ){
				currentCity = this.state.currentCity;
			}
			
			// Verificamos la ciudad
			if( typeof currentCity !== "string" ){
				throw new TypeError( "expected a string, but received a " + typeof currentCity );
			}

			// Obtenemos los datos.
			const response = await WeatherModel.getForecast( currentCity );

			// Verificamos los datos de la respuesta.
			if( response?.cod === "200" && Array.isArray( response?.list ) ){

				// Datos del cliente
				const forecast = this.generateSchemeForecast( response?.list );
				
				// Actualizamos el estado
				this.setState({ forecast });

			}

			return true;

		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return false;

		}

	}

	/**
	 * @desc Genero la estructura del pronostico futuro.
	 * 
	 * @param { Array } list Listado del Pronostico. 
	 * 
	 * @return { Array }
	 */
	generateSchemeForecast( list = [] ){

		try{

			// Verificamos la lista
			if( !Array.isArray( list ) ){
				throw new TypeError( "expected a Array, but received a " + typeof list );
			}

			// Configuración
			const intervalHours = 8;

			// Resultado 
			let listFiltered = this.filterForecastByHour( list, intervalHours );

			// Verificamos si obtuvimos un resultado correcto.
			if( !Array.isArray( listFiltered ) ){
				throw new TypeError( "Expected a var listFiltered of type array but received a type "+ typeof listFiltered );
			}

			// Parseamos los datos y retornamos.
			return listFiltered.map( forecast => ({
					"status": forecast?.weather[0]?.description,
					"date": Moment( forecast?.dt_txt ).format( "LL" ),
					"temp": {
						"min": KelvinToCelsius( forecast?.main?.temp_min ),
						"max": KelvinToCelsius( forecast?.main?.temp_max )
					}
				}) );
			
		}catch( error ){

			// Logueamos el error
			process.env.NODE_ENV !== "test" &&
				console.log( error );

			return [];

		}

	}

	/**
	 * @desc Filtramos el pronostico por horas.
	 * 
	 * @param { Array } list Listado del Pronostico. 
	 * @param { Number } intervalHours Intervalo de horas del listado. 
	 * 
	 * @return { Array }
	 */
	filterForecastByHour( list, intervalHours ){

		try{

			// Verificamos la lista.
			if( !Array.isArray( list ) ){
				throw new TypeError( "Expected a param list of type array but received a type "+ typeof list );
			}

			// Verificamos la lista.
			if( typeof intervalHours !== "number" ){
				throw new TypeError( "Expected a param intervalHours of type number but received a type "+ typeof list );
			}

			return list.filter(( forecastOfDay, index ) => ( intervalHours == 0 || index == 0 || index % intervalHours === 0) );

		}catch( error ){

			// Verificamos
				console.log( error );

			return [];

		}

	}

}

export default AppUI;
