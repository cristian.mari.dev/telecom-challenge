/**
 * @desc Dependencias
 */
import styled from "styled-components";

/**
 * @desc Material Design
 */
import { Grid } from "@material-ui/core";

/**
 * @desc Linea del tiempo
 */
export const Timeline = styled.div`
    width:-webkit-calc( 50% - 60px );
    border-left:1px solid #333;
    padding:0px 25px;
    @media (max-width: 768px) {
        &{
            width:-webkit-calc( 100% - 70px );
            margin-left:20px;
        }
    }    
    @media (max-width: 425px) {
        &{
            padding-top:40px;
        }
    }   
`;

/**
 * @desc Contenedor del día
 */
export const Day = styled.div`
    display:block;
    width: 100%;
    margin:10px 0px;
    position:relative;
    border-bottom:1px solid #ccc;
    padding-bottom:10px;
    &:after{
        display:block;
        background:#2f2f2f;
        width:10px;
        height:10px;
        content: " ";
        position: absolute;
        top: -webkit-calc( 50% - 20px );
        left:-30px;
        border-radius:50%;
    }
`;

/**
 * @desc Titulo del bloque
 */
export const Title = styled.h2`
    color:#333;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:1.5rem;
    margin:0px;
    margin-bottom:20px;
`;

/**
 * @desc Titulo: Fecha del día
 */
export const DateDay = styled.h2`
    color:#333;
    font-family:'IBM Plex Sans', sans-serif;
    margin:0px;
`;

/**
 * @desc Estado del Pronostico
 */
export const Forecast = styled.p`
    color:#555;
    font-family:'IBM Plex Sans', sans-serif;
    text-transform:Capitalize;
    margin:5px 0px;
`;

/**
 * @desc Contenedor de la temperatura
 */
export const Temp = styled(Grid)`
    color:#555;
    font-family:'IBM Plex Sans', sans-serif;
    margin:5px 0px;
`;


/**
 * @desc Temperatura minima
 */
export const Min = styled( Grid )`
    color:#555;
    font-family:'IBM Plex Sans', sans-serif;
    margin:0px;
`;


/**
 * @desc Estado del Pronostico
 */
export const Max = styled( Grid )`
    color:#555;
    font-family:'IBM Plex Sans', sans-serif;
    margin:0px 10px;
`;
