/**
 * @desc Dependencias
 */
import React, { useState } from 'react';
import Moment from "moment";

/**
 * @desc Utilidades
 */
import { KelvinToCelsius } from "../../../../Utils/Temperature";

/**
 * @desc MaterialUI
 */
import { Grid } from "@material-ui/core";

/**
 * @desc Componentes estilisados
 */
import { Background, DayDate, Temp, Location, Time, Content, Current } from "./styles.js";

/**
 * @desc Componentes
 * 
 * @param { Object } props
 * 
 * @return { HTMLSection }
 */
export default ({ ...props }) => {

    // Alias de las propiedades
    const {

        // Turno del día (Day|Night)
        turn = 'day',

        // Estado del clima
        status = '',

        // Datos del cliente
        client = null,

        // Ciudad actual
        currentCity = null,

        // Clima actual
        currentWeather,

        // Nodos hijos
        children

    } = props;

    // Indicamos el lenguaje
    Moment.locale( "es" );

    // Actualizamos la hora
    const [ date, useSetDate ] = useState( Moment().format("LL") );
    const [ time, useSetTime ] = useState( Moment().format("LT") );

    setInterval(() => {
        useSetDate(Moment().format("LL"));
        useSetTime(Moment().format("LT"));
    }, 5000);

    // Momento del día
    const isOfDay = currentWeather ? (new Date() > new Date(currentWeather.sunset * 1000) ? 'night' : 'day') : 'day';

    return (
        <Background data-turn={ isOfDay } data-status={ status }>

            { /* Contenedor superior */ }
            <Grid container={ true } justify={ "space-evenly" }>

                { /* Dia */ }
                <DayDate>
                    { date }<br />
                    <Time>{ time }</Time>
                </DayDate>

                { /* Ubicación */ }
                <Location>{ client?.province + ( currentCity && ", "+currentCity ) }</Location>

            </Grid>

            { /* Contenedor superior */ }
            <Grid container={ true } wrap={ "wrap" } justify={ "center" }>

                { /* Estado actual del clima */ }
                <Current> { currentWeather?.status ?? '' } </Current>

                { /* Temperatura */ }
                <Temp>{ currentWeather ? KelvinToCelsius(currentWeather?.temp) : '' } <sup>C</sup></Temp>

            </Grid>

            { /* Contenedor */ }
            <Content container={ true } justify={ "center" }>{ children }</Content>

        </Background>
    );

};