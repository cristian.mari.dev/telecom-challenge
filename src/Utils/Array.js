/**
 * @desc Obtiene n valores de un array de forma aleatoria.
 * 
 * @param { Array } arr 
 * @param { Number } max
 * 
 * @return { Array } 
 */
export const getRandomArray = (arr,  max ) => {

    var result = new Array( max ),
        len = arr.length,
        taken = new Array(len);
    if (max > len)
        throw new RangeError("getRandom: se indicaron mas elementos de los que hay disponible.");
    while (max--) {
        var x = Math.floor(Math.random() * len);
        result[max] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;

}

