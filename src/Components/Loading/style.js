/**
 * @desc Dependencias
 */
import styled from "styled-components";

/**
 * @desc Material Design
 */
import { Grid, CircularProgress } from "@material-ui/core";

/**
 * @desc Capa de fondo
 */
export const Overlay = styled.div`
    display:flex;
    background:white;
    width: 100%;
    height:100%;
    position:fixed;
    top:0; left:0;
    z-index:2;
    justify-content:center;
    align-items:center;
    text-align:center;
`;

/**
 * @desc Estilos del contenedor
 */
export const Content = styled(Grid)`
`;

/**
 * @desc Estilos del titulo
 */
export const Title = styled.h1`
    color:#333;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:1.5rem;
`;

/**
 * @desc Estilos del spinner
 */
export const Spinner = styled( CircularProgress )`
`;