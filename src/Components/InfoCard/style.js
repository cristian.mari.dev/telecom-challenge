/**
 * @desc Dependencias
 */
import styled from "styled-components";

/**
 * @desc Material Design
 */
import { Grid } from "@material-ui/core";

/**
 * @desc Estilos de la carta
 */
export const InfoCard = styled(Grid)`
    background:-webkit-linear-gradient(top, #1b9ba1, #503ba0);
    flex-basis: -webkit-calc( 25% - 10px ) !important;
    padding:20px 10px;
    margin:10px;
    border-radius:4px;
    @media (max-width: 425px) {
        &{
            flex-basis: -webkit-calc( 50% - 10px ) !important;
            margin: 2px  0px !important;
            padding:10px;
        }
    }    
`;

/**
 * @desc Estilos de la carta
 */
export const Title = styled.h2`
    color:white;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:0.8rem;
    font-weight:400;
    text-align:center;
    text-transform:capitalize;
    @media (max-width: 768px) {
        &{
            font-size:12px;
        }
    }    
`;

/**
 * @desc Estilos de la carta
 */
export const Content = styled.p`
    color:white;
    font-family:'IBM Plex Sans', sans-serif;
    font-size:3rem;
    text-align:center;
    margin:5px 0px;
    @media (max-width: 768px) {
        &{
            font-size:14px;
        }
    }    
`;