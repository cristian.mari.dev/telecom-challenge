
/**
 * @desc Modelo
 */
class IPModel {

    /**
     * @desc Solicita datos a la api de ip 
     * 
     * @return { Promise<Boolean> }
     */
    static getIp(){

        return new Promise(( resolve, reject ) => {

            try{

                // Solicitud
                fetch("https://ipapi.co/json")
                    .then( response => resolve( response.json() ) )
                    .catch( reject );

                return true;

            }catch( error ){

                // Logueamos el error
                console.log( error );

                return false;

            }

        });

    }

}

export default IPModel;